﻿using System;

namespace ex141
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, please type in Birth month");

            var num1 = int.Parse(Console.ReadLine());

            Console.WriteLine($"The number you typed in multiplied by 2 is {num1 * 2}");
        }
    }
}